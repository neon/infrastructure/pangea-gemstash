#!/bin/sh
#
# Copyright © 2017 Harald Sitter <sitter@kde.org>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2 of
# the License or (at your option) version 3 or any later version
# accepted by the membership of KDE e.V. (or its successor approved
# by the membership of KDE e.V.), which shall act as a proxy
# defined in Section 14 of version 3 of the license.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Define a timestamp function
timestamp() {
  date +"%F_%T" # current time
}

# set up stack for making our own gems
set -ex
export GEM_HOME=$(ruby -e 'puts Gem.user_dir')
export GEM_PATH=$GEM_HOME:$HOME/.gems/bundler
export PATH=$GEM_HOME/bin:$PATH
if [ ! -f ~/.gemrc ]; then
    echo 'gem: --no-document' > ~/.gemrc
fi

# install required system deps for gemstash
#apt install libsqlite3-dev
#apt install memcached

# install, verify and upgrade bundler
gem install bundler --conservative
gem update bundler

#install the config before we gemstash
if [ -f /home/gemstash/.gemstash/config.yaml ]; then
    rm -vf /home/gemstash/.gemstash/config.yaml
fi
if [ -d "/home/gemstash/.gemstash/" ]
then
    echo "~/.gemstash/ exists"
    cp -vf config.yaml /home/gemstash/.gemstash/config.yaml
    if [ -f /home/gemstash/.gemstash/gemstash.log ]; then
        mv -v /home/gemstash/.gemstash/gemstash.log /home/gemstash/.gemstash/gemstash.log.$(timestamp)
        echo "existing gemstash log, backing it up."
    fi
else
    echo "~/.gemstash/ doesn't exist, lets start from the beginning"
    mkdir -v /home/gemstash/.gemstash/
    cp -vf config.yaml /home/gemstash/.gemstash/config.yaml
fi

# install, configure and start gemstash
gem install gemstash
# puma >= 5 has no daemonize option anymore
gemstash start --no-daemonize --config-file /home/gemstash/.gemstash/config.yaml
#bundle install
#bundle update --bundler


# maybe use bundle exec in the future
#gem exec gemstash start --no-daemonize
