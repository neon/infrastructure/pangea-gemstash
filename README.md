# Purpose

Gemstash is both a cache for remote servers such as https://rubygems.org, and
a private gem source..

For our purposes we use as an on-site cache of gems and also as native host of
from-git built gems. Because Bundler's git gemming isn't working particularly
well for use we're building gems we need from git elsewhere and push them
into the box so bundler can treat these gems just like any other gem.

# Provision

Gemstash is provisioned by our kitchen.

# start.sh

It's the entry point of the systemd service. It does some initial startup
updating to make sure the stack is up to date (and hopefully secure). Once
updated start.sh will gem install and start gemstash.

# config.yaml

Gemstash looks for for config.yaml for configuration instructions. Puma is
bundled as the (virtually unconfigurable) default web server for gemstash
(which in turn is proxied by apache in deployment scenarios). config.yaml
configures it and other db related options and is copied to
~/.gemstash/config.yaml during the start.sh bootstrap.  Also, an api key
based authentication mechanism is added to the core functionality. The
authentication credentials are stored in ~/.config/pangea_build_gem.yaml

# Auth

For API usage we use a key for use in ~/.gem/credentials on the client-side.

# Git-builds

Jenkinsfile and build_gem.rb facilitiate building of a gem from git. Jenkinsfile
assembles the repos and uses build_gem to actually build and push the gem.

## build_gem

Building is fairly straight forward. We patch the `*.gemspec` found in the gem
repo to hijack the version definition. We'll then append a `.$date.$time` suffix
to differentiate our builds from potential upstream ones. Additionally this
ensures different versions across rebuilds. The datetime itself is derived from
the latest git commit and as such is persistent across rebuilds of the same
HEAD (and hopefully monotone increasing).

Once we have a gem we'll conduct some additional assertations to make sure our
build is in fact newer than what is in the box already.

If all is fine we `gem push` to our box. This requires ~/.gem/credentials to be
set up with our api key.
